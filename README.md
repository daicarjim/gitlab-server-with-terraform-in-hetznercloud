# Terraform in HetznerCloud-Deploy-Gitlab-Server

Automate the provisioning of infrastructure resources in Hetzner-Cloud by using Terraform and the deployment GITLAB.


## How to use

## Requirements 📋

WARNING: TEST/UBUNTU 18.04 in local machine

The following commands run on your ubuntu 18.04 local machine

```
apt update
```
```
apt install git
```
```
apt install unzip
```
```
ssh-keygen
```
Then...
 
1) Install Terraform 0.12
```
wget https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip
```
```
unzip terraform_0.12.24_linux_amd64.zip
```
```
mv terraform /usr/local/bin
```
```
terraform version
```

## Clone this repository 📄

```

git clone https://gitlab.com/daicarjim/gitlab-server-with-terraform-in-hetznercloud.git
```


## Hetzner Cloud Token 📖

To provision the infrastructure where Gitlab-Server will be installed, it is necessary to have an account in Hetzner Cloud. Once we have the account, we must give Terraform access so that it can create the infrastructure for us.

For this it is necessary to generate a token, go to the page: https://colinwilson.uk/2020/10/31/generate-an-api-token-in-hetzner-cloud/ this is a short-tutorial and create it.


## Create FILE with name terraform.tfvars(variables file)🔧

1) enter the main project folder
```
cd gitlab-server-with-terraform-in-hetznercloud
```
2) with command:
```
vim terraform.tfvars
```
3) Copy and paste this EXAMPLE in your file terraform.tfvars and CHANGE VARIABLES what your needed:


**within file terraform.tfvars**
```
hcloud_token = "COPY-YOUR-TOKEN-HETZNERCLOUD"

ssh_key_private = "~/.ssh/id_rsa"

ssh_public_key = "~/.ssh/id_rsa.pub"

server_name = "Gitlab-Server"

server_size = "cpx21"

server_region = "nbg1"
```


To deploy Gitlab-Server we just have to execute the following commands:
```
terraform init
```
```
terraform plan
```
```
terraform apply 
```
(write "yes" when terraform asks you)


_NOTE:When you want eliminate resources only should execute:_

```
terraform destroy
```

# GITLAB TEST-MODE ⚙️

If you want Gitlab in test-mode then execute the next

1) login your machine Gitlab
```
ssh -p22 root@IP-MACHINE-GITLAB
```
and
```
sudo gitlab-ctl reconfigure
```
Then when terminate the process go to IP-MACHINE-GITLAB in the browser.


# GITLAB PRODUCTION-MODE ⚙️

But if you want Gitlab in production then follow the next:

Create first (freenom.com) a domain for your site.

configure DNS
```
NAME         TYPE    TTL     TARGET
              A      3600    IP-MACHINE-GITLAB
gitlab        A      3600    IP-MACHINE-GITLAB
www           A      3600    IP-MACHINE-GITLAB
```
In register glue records
```
NS1           IP-MACHINE-GITLAB
NS2           IP-MACHINE-GITLAB
NS3           IP-MACHINE-GITLAB
```
1) Access to your machine Gitlab
```
ssh -p22 root@IP-MACHINE-GITLAB
```
2) Execute the next command:
```
sudo apt install postfix
```
_A box will appear enter and OK_

_Then choose "internal site"_

![alt text](https://www.howtoforge.com/images/how_to_install_and_configure_gitlab_on_ubuntu_1804/big/2.png)

_And  write name of YOURDOMAIN.COM_

![alt text](https://www.howtoforge.com/images/how_to_install_and_configure_gitlab_on_ubuntu_1804/big/3.png)

3) access to document /etc/gitlab/gitlab.rb 
```
sudo vim /etc/gitlab/gitlab.rb 
```

4) search with (/) the word external and configure your domain and add "s" to the http (Remember that in VIM you have press letter (i) for do changes)

```
external_url "https://gitlab.YOURDOMAIN.COM"
```

5) search section letsencrypt, nginx, external then enable and modify:

```
letsencrypt['enable'] = true
```
```
letsencrypt['contact_emails'] = ['email@YOURDOMAIN.COM']
```
```
letsencrypt['auto_renew'] = true
```
```
letsencrypt['auto_renew_hour'] = "12"
```
```
letsencrypt['auto_renew_minute'] = "30"
```
```
letsencrypt['auto_renew_day_of_month'] = "*/7"
```
```
nginx['redirect_http_to_https'] = true
```
```
nginx['ssl_certificate'] = "/etc/gitlab/ssl/gitlab.YOURDOMAIN.COM.crt"
```
```
nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/gitlab.YOURDOMAIN.COM.key"
```
```
nginx['ssl_dhparam'] = "/etc/gitlab/ssl/dhparams.pem"
```
```
registry_external_url 'https://gitlab.YOURDOMAIN.COM:5050'
```

```
gitlab_rails['registry_enabled'] = true
```

```
gitlab_rails['registry_host'] = "gitlab.YOURDOMAIN.COM"
```

```
gitlab_rails['registry_port'] = "5050"
```

```
gitlab_rails['registry_path'] = "/var/opt/gitlab/gitlab-rails/shared/registry"
```

```
registry['enable'] = true
```

```
registry['dir'] = "/var/opt/gitlab/registry"
```

```
registry['log_directory'] = "/var/log/gitlab/registry"
```

```
registry['env_directory'] = "/opt/gitlab/etc/registry/env"
```

## then save your changes with :wq and enter 

6) Now execute the command: 
 
```
sudo gitlab-ctl reconfigure  

```

# you have wait for 10 minutes ⏰


Now go to "gitlab.YOURDOMAIN.COM" in your browser


_Create password_

![alt text](https://assets.digitalocean.com/articles/gitlab_install_1604/gitlab_initial_password2.png)


_The user default is root and enter your password_


![alt text](https://assets.digitalocean.com/articles/gitlab_install_1604/gitlab_first_signin2.png)


_For the moment if you want configure container registry public but with access only for members. See... Settings/General/Visibility,project features, permissions._


![alt text](Tutorial configure GITLAB.png)
 


# CONGRATULATIONS now you have Gitlab in production-mode!!!!  


# GRATITUDE 🎁

```
https://github.com/galvarado/terraform-ansible-DO-deploy-wordpress
https://www.digitalocean.com/community/tutorials/como-instalar-y-configurar-gitlab-en-ubuntu-18-04-es
https://www.howtoforge.com/tutorial/how-to-install-and-configure-gitlab-on-ubuntu-1804/
https://docs.gitlab.com/omnibus/settings/ssl.html#lets-encrypt-fails-on-reconfigure
https://noviello.it/es/como-proteger-el-servidor-de-gitlab-con-lets-encrypt-ssl/
```

⌨️ Esto se hizo con el ❤️ para la comunidad por [daicarjim](https://gitlab.com/daicarjim) 👍
