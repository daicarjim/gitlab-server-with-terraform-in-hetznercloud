variable "hcloud_token" {}
variable "ssh_key_private" {}
variable "ssh_public_key" {}
variable "server_name" {}
variable "server_size" {}
variable "server_region" {}

# Configure the Hetzner Provider
provider "hcloud" {
    token = var.hcloud_token
}

resource "hcloud_ssh_key" "k8s_admin" {
  name       = "k8s_admin"
  public_key = file(var.ssh_public_key)
}



# Create a web server
resource "hcloud_server" "myblog" {
    image         = "ubuntu-18.04"
    name          = var.server_name
    location      = var.server_region
    server_type   = var.server_size
    ssh_keys      = [hcloud_ssh_key.k8s_admin.id]

    # Install package basics on the droplet using remote-exec to execute ansible playbooks to configure the services
    provisioner "remote-exec" {
        inline = [
          "apt update -yq",
          "apt install git -yq",
          "sudo apt-get install -y fail2ban",
          "sudo apt install curl -y",
          "sudo apt-get install -y curl openssh-server ca-certificates",
          "curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash",
          "apt update -yq",
          "sudo apt install gitlab-ce -y",
          "sudo ufw --force enable",
          "ufw allow http",
          "ufw allow https",
          "ufw allow OpenSSH",
          "ufw allow 5050",
          "ufw allow 5000",
          "sudo mkdir -p /etc/gitlab/ssl/",
          "sudo openssl dhparam -out /etc/gitlab/ssl/dhparams.pem 2048",
          "chmod 600 /etc/gitlab/ssl/*"
 
        ]

         connection {
            host        = self.ipv4_address
            type        = "ssh"
            user        = "root"
            private_key = file(var.ssh_key_private)
        }
   }

}

